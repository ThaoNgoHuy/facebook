/* eslint-disable prettier/prettier */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import AsyncStorage from '@react-native-community/async-storage';
import React, { useEffect} from 'react';
import {View, Text} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import MainStackNavigator from './src/navigation/MainStackNavigator';
import AuthStackNavigator from './src/screens/auth/navigation/AuthStackNavigator';
import SplashScreen from './src/screens/SplashScreen';
import { restoreToken } from './src/slices/authSlice';

const App = () => {
  const isLoading = useSelector(state => state.auth.isLoading);
  const userToken = useSelector(state => state.auth.userToken);
  const dispatch = useDispatch();

  useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let token;

      try {
        token = await AsyncStorage.getItem('userToken');
        console.log('token: ', token);
      } catch (e) {
        // Restoring token failed
        console.log('Restoring token failed', e);
      }

      // After restoring token, we may need to validate it in production apps

      // This will switch to the App screen or Auth screen
      // and this loading screen will be unmounted and thrown away
      const action = restoreToken({
        token: token,
      });
      dispatch(action);
    };

    bootstrapAsync();
  }, []);

  // const [productList, setProductList] = useState([]);
  // useEffect(() => {
  //   const fetchProductList = async () => {
  //     try {
  //       const params = 1;
  //       const response = await productApi.get(params);
  //       console.log('Fetch products successfully: ', response);
  //       // setProductList(response.data);
  //     } catch (error) {
  //       console.log('Failed to fetch product list: ', error);
  //     }
  //   }
  //   fetchProductList();
  // }, []);

  // useEffect(() => {
  //   // const storeData = async (value) => {
  //   //   try {
  //   //     await AsyncStorage.setItem('testkey', value);
  //   //   } catch (e) {
  //   //     // saving error
  //   //     console.log('coloi');
  //   //   }
  //   // };

  //   // storeData('testvalue');

  //   const getData = async () => {
  //     try {
  //       const value = await AsyncStorage.getItem('testkey');

  //       console.log('value: ', value);

  //       if (value !== null) {
  //         // value previously stored
  //       }
  //     } catch (e) {
  //       // error reading value

  //       console.log('zzz');
  //     }
  //   };

  //   getData();
  // }, []);
  // if (isLoading) {
  //   return (
  //     <SplashScreen />
  //   );
  // }

  // if (!userToken) {
  //   return (
  //     <AuthStackNavigator />
  //   );
  // }

  return(
    <MainStackNavigator />
  );

};
export default App;
