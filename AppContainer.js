/* eslint-disable prettier/prettier */
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { Provider } from 'react-redux';
import { StyleSheet, Text, TouchableOpacity, View, TextInput, Image, ScrollView, FlatList, Dimensions } from 'react-native';
import App from './App';
import store from './src/app/store';
import Video from 'react-native-video';
import {useRef , useState} from 'react';



function AppContainer(props) {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <App />
      </NavigationContainer>
    </Provider>
  );
}
export default AppContainer;


