/* eslint-disable prettier/prettier */
import axiosClient from './axiosClient';

const authApi = {
  signup: (params) => {
    const url = '/user/signup';
    return axiosClient.post(url, params);
  },
};

export default authApi;
