import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, TextInput, Image, ScrollView, FlatList, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as colors from '../constants/colors';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import {useRef , useState} from 'react';
import { useLinkProps, useNavigation } from '@react-navigation/native';
import Collapsible from 'react-native-collapsible';
import Hr from "react-native-hr-component";

const windowWidth = Dimensions.get('window').width;


export default function ImageGrid({length, images}){
    if(length === 1){
        return(
            <View style={styles.imgContainer}>
                <Image source={{uri : images[0] }} style={{width: windowWidth, height: windowWidth}}></Image>
            </View>
        );
    }
    else if(length === 2){
        return(
            <View style={styles.imgContainer}>
                <Image source={{uri : images[0] }} style={{width: windowWidth/2, height: windowWidth, marginRight: 2}}></Image>
                <Image source={{uri : images[1] }} style={{width: windowWidth/2, height: windowWidth}}></Image>
            </View>
        );
    }
    else if(length === 3){
        return(
            <View style={styles.imgContainer}>
                <Image source={{uri : images[0] }} style={{width: windowWidth/2, height: windowWidth, marginRight: 2}}></Image>
                <View>
                    <Image source={{uri : images[1] }} style={{width: windowWidth/2, height: windowWidth/2, marginBottom: 2,}}></Image>
                    <Image source={{uri : images[2] }} style={{width: windowWidth/2, height: windowWidth/2 }}></Image>
                </View>
            </View>
        );
    }
    else if(length === 4){
        return(
            <View style={styles.imgContainer}>
                <View style={{marginRight: 2}}>
                    <Image source={{uri : images[0] }} style={{width: windowWidth/2, height: windowWidth/2, marginBottom: 2,}}></Image>
                    <Image source={{uri : images[1] }} style={{width: windowWidth/2, height: windowWidth/2 }}></Image>
                </View>
                <View>
                    <Image source={{uri : images[2] }} style={{width: windowWidth/2, height: windowWidth/2, marginBottom: 2,}}></Image>
                    <Image source={{uri : images[3] }} style={{width: windowWidth/2, height: windowWidth/2 }}></Image>
                </View>
            </View>
        );
    }
    else return null;
}

const styles = StyleSheet.create({

    imgContainer: {
        flexDirection: 'row',
        marginVertical: 5,
    }

});