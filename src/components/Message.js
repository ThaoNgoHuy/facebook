import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, TextInput, Image, ScrollView, Button, FlatList, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {useRef , useState} from 'react';
import SwipeUpDownModal from 'react-native-swipe-modal-up-down';

const { width } = Dimensions.get('window');


export default function Message({item}){

    const [ShowComment, setShowModelComment] = useState(false);
    const [animateModal, setanimateModal] = useState(false);

    return(
        <View>
            <TouchableOpacity onLongPress={() => setShowModelComment(true)}>
                <View style={styles.container}>
                    <View style={styles.bigAvatar}>
                        <Image 
                            source={{uri: item.avatar}}
                            style={styles.avatar}
                        />
                    </View>
                    <View style={styles.info}>
                        <Text style={styles.name}>{item.name}</Text>
                        <Text numberOfLines={1} style={{fontSize: 15,}}>{item.description}</Text>
                    </View>
                    <View style={styles.bigSeen}>
                        <Image 
                            source={{uri: item.avatar}}
                            style={styles.avatarSeen}
                        />
                    </View>
                </View>
            </TouchableOpacity>
            <SwipeUpDownModal
                modalVisible={ShowComment}
                PressToanimate={animateModal}
                ContentModal= {
                    <View style={styles.containerContent}>
                        <View style={styles.topContent}>
                            <Ionicons name={'remove'} size={30} />
                        </View>
                        <View style={styles.midContent}>
                            <TouchableOpacity style={styles.buttonContent}>
                                <Ionicons name={'ios-archive'} size={25} />
                                <Text style={styles.textButton}>Luu tru</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.midContent}>
                            <TouchableOpacity style={styles.buttonContent}>
                                <Ionicons name={'trash'} size={25} />
                                <Text style={styles.textButton}>Xoa</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.midContent}>
                            <TouchableOpacity style={styles.buttonContent}>
                                <Ionicons name={'notifications-off'} size={25} />
                                <Text style={styles.textButton}>Tat thong bao</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.midContent}>
                            <TouchableOpacity style={styles.buttonContent}>
                                <Ionicons name={'chatbubbles'} size={25} />
                                <Text style={styles.textButton}>Mo bong bong chat</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.midContent}>
                            <TouchableOpacity style={styles.buttonContent}>
                                <Ionicons name={'mail'} size={25} />
                                <Text style={styles.textButton}>Danh dau la chua doc</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.midContent}>
                            <TouchableOpacity style={styles.buttonContent}>
                                <Ionicons name={'ios-cloud-offline'} size={25} />
                                <Text style={styles.textButton}>Bo qua tin nhan</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.midContent}>
                            <TouchableOpacity style={styles.buttonContent}>
                                <Ionicons name={'ios-remove-circle'} size={25} />
                                <Text style={styles.textButton}>Chan</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                }
                ContentModalStyle={styles.Modal}
                onClose={() => {
                    setShowModelComment(false);
                    setanimateModal(false);
                }}
            >
            </SwipeUpDownModal>
        </View>
    );
};

const styles = StyleSheet.create({

    container: {
        flexDirection: 'row',
        paddingLeft: 5,
        paddingVertical: 5,
        marginBottom: 5,
    },
    bigAvatar: {
        flex: 2
    },
    avatar:{
        width: width*15/100,
        height: width*15/100,
        borderRadius: width*10/100,
    },
    info: {
        flex: 8,
        flexDirection: 'column',
        paddingLeft: 10,
        justifyContent: 'center'

    },
    name: {
        fontWeight: 'bold',
        color: 'black',
        fontSize: 17,
        paddingBottom: 3
    },
    bigSeen: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 2,        
    },
    avatarSeen: {
        width: width/20,
        height: width/20,
        borderRadius: width/20,
    },
    item: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 100,
    },
    Modal: {
        backgroundColor: '#fff',
        marginTop: width*2/3,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
    },
    containerContent: {

    },
    topContent: {
        alignItems: 'center',
        paddingTop: 5,
    },
    midContent: {
        justifyContent: 'center',
    },
    buttonContent: {
        flexDirection: 'row',
        paddingHorizontal: 30,
        alignItems: 'center',
        paddingVertical: 11,
    },
    textButton: {
        fontSize: 16,
        marginLeft: 20,
    }
});