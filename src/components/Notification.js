import { Text, View , Image, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import React, {Component , useState} from 'react';
import {  Overlay } from 'react-native-elements';

const windowWidth = Dimensions.get('window').width;


export default function Notation({avatar, content, time}){

	const [visible, setVisible] = useState(false);

	const toggleOverlay = () => {
		setVisible(!visible);
	}

	return(
		<View>
			<TouchableOpacity style={styles.buttonContainer}>
				<View style={styles.imageContainer}>
					<Image style={styles.avatar} source={require('./../../assets/images/huyhoang.jpg')} />
					<View style={styles.iconContainer}>
						<Ionicons name={'notifications-circle'} size={30} color={'#80ff00'}  />
					</View>
				</View>
				<View style={styles.contentContainer}>
					<Text style={styles.content} ellipsizeMode='tail' numberOfLines={3} >{content}</Text>
					<Text style={styles.time}>{time}</Text>
				</View>
				<TouchableOpacity style={styles.setting} onPress={toggleOverlay}>
					<Ionicons name={'ellipsis-horizontal'} size={24} />
				</TouchableOpacity>
			</TouchableOpacity>
			<Overlay isVisible={visible} onBackdropPress={toggleOverlay}>
				<View style={styles.overlay}>
					<View style={styles.alert}>
						<Image style={styles.avatarAlert} source={require('./../../assets/images/huyhoang.jpg')} />
						<Text style={styles.contentAlert} ellipsizeMode='tail' numberOfLines={2}>{content}</Text>
					</View>
					<TouchableOpacity style={styles.removeButton}>
						<Ionicons name={'close-circle-sharp'} size={28} />
						<Text style={styles.removeAllert}>Gỡ thông báo này</Text>
					</TouchableOpacity>
				</View>
			</Overlay>
		</View>
	);
};

const styles = StyleSheet.create({

	buttonContainer: {
		flexDirection : 'row',
		alignItems : 'stretch',
		padding : 10,
	},
	imageContainer: {
	},
	avatar: {
		width : windowWidth/7,
		height : windowWidth/7,
		borderRadius : windowWidth/7,
	},
	iconContainer: {
		position: 'absolute',
		right : -2,
		top : 38,
		backgroundColor : '#fff',
		borderRadius : 100,
	},
	contentContainer: {
		flexDirection : 'column',
		justifyContent : 'flex-start',
		paddingLeft : 10,
		width : windowWidth*5/7
	},
	content: {
		fontSize : 18,
	},
	time: {
		fontSize : 15,
		fontWeight : 'bold',
	},
	setting: {
		alignItems : 'flex-end',
		paddingLeft : 10,
		justifyContent : 'flex-start',
	},
	overlay: {
		flexDirection : 'column',
		width : windowWidth,
	},
	alert: {
		alignItems: 'center',
		flexDirection : 'column',
	},
	avatarAlert: {
		width : windowWidth/8,
		height : windowWidth/8,
		borderRadius : windowWidth/8,
	},
	contentAlert: {
		fontSize : 15,
		paddingLeft : 20,
		paddingRight : 20,
		paddingTop : 5,
	},
	removeButton: {
		flexDirection : 'row',
		paddingLeft : 10,
		paddingTop : 30,
		paddingBottom : 20,
	},
	removeAllert: {
		paddingLeft : 10,
		fontWeight: 'bold',
		fontSize: 18,
	}
});