import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, TextInput, Image, ScrollView, FlatList, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {useRef , useState} from 'react';
import ImageGrid from './ImageGrid';
import VideoComponent from './VideoComponent';

const windowWidth = Dimensions.get('window').width;

export default function Posts({ data }){

    const [is_liked, setIs_liked] = useState(data.posts.is_liked);

    return(
        <View style={styles.container}>
            <View style={styles.top}>
                <View style={styles.user}>
                    <TouchableOpacity><Image source={require('../../assets/images/huyhoang.jpg')} style={styles.image} /></TouchableOpacity>
                    <View>
                        <TouchableOpacity><Text style={styles.name}>{data.name}</Text></TouchableOpacity>
                        <Text style={styles.time}>{data.posts.created}</Text>
                    </View>
                </View>
                <TouchableOpacity>
                    <Ionicons name={'ios-ellipsis-horizontal-sharp'} size={27} />
                </TouchableOpacity>
            </View>
            <View style={styles.mid}>
                <Text style={styles.content}>{data.posts.content}</Text>
                <ImageGrid length={data.posts.image.length} images={data.posts.image} />
            </View>
            <View style={styles.bottom}>
                <View style={styles.element}>
                    <View style={styles.like}>
                        <Ionicons />
                        <Text></Text>
                    </View>
                    <Text>{data.posts.comment}</Text>
                </View>
                <View style={styles.likeBar}>
                    <TouchableOpacity style={styles.button} onPress={() => setIs_liked(!is_liked)}>
                        <Ionicons name={is_liked ? 'thumbs-up' : 'thumbs-up-outline'} size={20} />
                        <Text style={styles.content} >  Thich</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button}>
                        <Ionicons name={'chatbox-outline'} size={20} />
                        <Text style={styles.content} >  Binh luan</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button}>
                        <Ionicons name={'arrow-redo-outline'} size={20} />
                        <Text style={styles.content}>  Chia se</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );

};

const styles = StyleSheet.create({

    container: {
        paddingVertical: 10,
        backgroundColor: '#fff',
        borderRadius: 10,
        marginBottom: 5,
        marginTop: 5,
    },
    top: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    user:{
        flexDirection: 'row',
    },
    image: {
        width: windowWidth/10,
        height: windowWidth/10,
        borderRadius: windowWidth/10,
        marginRight: 10,
    },
    name: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    time: {
        fontSize: 14,
    },  
    mid: {
        marginVertical: 10,
    },
    content: {
        fontSize: 17,
        paddingHorizontal: 5,
    },
    imgContainer:{
        flexDirection: 'row',
    },  
    bottom: {
    },
    element: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 15,
    },
    like: {
        flexDirection: 'row',
    },
    likeBar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingTop : 15,
    },
    button: {
        flexDirection: 'row',
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
    
});