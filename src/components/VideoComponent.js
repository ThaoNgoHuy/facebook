import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, TextInput, Image, ScrollView, FlatList, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as colors from '../constants/colors';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import {useRef , useState} from 'react';
import { useLinkProps, useNavigation } from '@react-navigation/native';
import Collapsible from 'react-native-collapsible';
import Hr from "react-native-hr-component";
import Video from 'react-native-video';

export default function VideoComponent({video, length}){

        // return(
        //     // <Video 
        //     //     source = {{uri : 'https://www.youtube.com/watch?v=5gBeLN2Jkng&list=RDMMl2UiY2wivTs&index=7'}}
        //     // />
        //     <View><Text>abc</Text></View>
        // ); 
    
    if(length === 0) return null;
    else return(
        // <View><Text>{video[0]}</Text></View>
        <View>
            <Video source={{uri: video[0]}} />
        </View>
    );
    
}