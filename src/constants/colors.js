/* eslint-disable prettier/prettier */
export const white = '#fff';

// PostCard
export const blueA400 = '#2979ff';
export const blueGrey100 = '#cfd8dc';
export const grey700 = '#616161';
export const grey900 = '#212121';

export const red600 = '#e53935';
export const lightGreen500 = '#8bc34a';
export const purple400 = '#ab47bc';
// PostCard

// SelectAccountScreen
export const blue50 = '#e3f2fd';
export const blue800 = '#1565C0';
// SelectAccountScreen
