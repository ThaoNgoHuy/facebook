/* eslint-disable prettier/prettier */
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import NavBar from '../components/NavBar';
import TopTabNavigator from './TopTabNavigator';
import StatusScreen from '../screens/StatusScreen';
import { StyleSheet, Button, View, SafeAreaView, Text, Alert } from 'react-native';
import SettingAccountScreen from '../screens/SettingAccountScreen';
import ChangePassScreen from '../screens/ChangePassScreen';
import YourInfoSetting from '../screens/YourInfoSetting';
import AnnounSettingScreen from '../screens/AnnounSettingScreen';
import CommentSettingScreen from '../screens/CommentSettingScreen';
import PushNotificationScreen from '../screens/PushNotificationScreen';


const Stack = createStackNavigator();

function MainStackNavigator() {
  return (
    <Stack.Navigator initialRouteName="TopTabNavigator">
      <Stack.Screen
        name="TopTabNavigator"
        component={TopTabNavigator}
        options={{
          headerTitle: () => <NavBar />,
        }}
      />
      <Stack.Screen 
        name="StatusScreen"
        component={StatusScreen}
        options={{
          headerTitle : 'Dang bai',
          headerRight : () => (
            <Button title="Dang" 
                    color="blue"
                    onPress={() => alert('This is a button!')}
            >
            </Button>
          ),
        }}
      />
      <Stack.Screen 
        name="SettingAccountScreen"
        component={SettingAccountScreen}
        options={{
          headerTitle : ''
        }}
      />
      <Stack.Screen
        name="ChangePassScreen"
        component={ChangePassScreen}
        options={{
          headerTitle : 'Đổi mật khẩu'
        }}
      />
      <Stack.Screen 
        name="YourInfoSetting"
        component={YourInfoSetting}
        options={{
          headerTitle : '',
        }}
      />
      <Stack.Screen 
        name="AnnounSettingScreen"
        component={AnnounSettingScreen}
        options={{
          headerTitle : 'Cài đặt thông báo',
        }}
      />
      <Stack.Screen 
        name="CommentSettingScreen"
        component={CommentSettingScreen}
        options={{
          headerTitle: 'Bình luận',
        }}
      />
      <Stack.Screen 
        name="PushNotificationScreen"
        component={PushNotificationScreen}
        options={{
          headerTitle: 'Đẩy',
        }}
      />
    </Stack.Navigator>
  );
}

export default MainStackNavigator;
