/* eslint-disable prettier/prettier */
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HomeScreen from '../screens/HomeScreen';
import NotificationScreen from '../screens/NotificationScreen';
import SettingScreen from '../screens/SettingScreen';
import * as colors from './../constants/colors';
import FriendScreen from '../screens/FriendScreen';
import Messenger from '../screens/Messenger';

const Tab = createMaterialTopTabNavigator();

function TopTabNavigator() {
  return (
    <Tab.Navigator
      initialRouteName={'Home'}
      tabBarOptions={{
        activeTintColor: colors.blueA400,
        inactiveTintColor: colors.grey700,
        showIcon: true,
        showLabel: false,
      }}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color }) => {
          let iconName;
          if (route.name === 'Home') {
            iconName = focused ? 'home' : 'home-outline';
          } else if (route.name === 'Notification') {
            iconName = focused ? 'notifications' : 'notifications-outline';
          } else if (route.name === 'Setting') {
            iconName = focused ? 'menu' : 'menu-outline';
          }
          else if (route.name === 'Friend') {
            iconName = focused ? 'people' : 'people-outline';
          }
          else if (route.name === 'Messenger') {
            iconName = focused ? 'chatbox' :'chatbox-outline';
          }
          return <Ionicons name={iconName} color={color} size={24} />;
        },
      })}
    >
      <Tab.Screen
        name="Home"
        component={HomeScreen}
      />
      <Tab.Screen
        name="Messenger"
        component={Messenger}
      />
      <Tab.Screen 
        name="Friend"
        component={FriendScreen}
      />
      <Tab.Screen
        name="Notification"
        component={NotificationScreen}
      />
      <Tab.Screen
        name="Setting"
        component={SettingScreen}
      />
    </Tab.Navigator>
  );
}

export default TopTabNavigator;
