import { Form, Input, Item, Label } from 'native-base';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, TextInput, Image, ScrollView } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as colors from './../constants/colors';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import {useRef , useState} from 'react';
import { useNavigation } from '@react-navigation/native';
import Collapsible from 'react-native-collapsible';
import Hr from "react-native-hr-component";


export default function AnnounSettingScreen(){

	const navigation = useNavigation();

	return(

		<ScrollView>
			<View style={styles.container}>
				<Text style={styles.firstText}>Bạn muốn nhận thông báo về </Text>
				<View style={styles.containerChild}>
					<TouchableOpacity style={styles.button} onPress={() => navigation.navigate('CommentSettingScreen')}>
						<Ionicons name={'ios-chatbox-outline'} size={25} />
						<View style={styles.containerLabel}>
							<Text style={styles.label}>Bình luận</Text>
							<Text style={styles.text}>Thông báo đẩy,email,SMS</Text>
						</View>
					</TouchableOpacity>
				</View>
				<View style={styles.containerChild}>
					<TouchableOpacity style={styles.button}>
						<Ionicons name={'pricetag-outline'} size={25} />
						<View style={styles.containerLabel}>
							<Text style={styles.label}>Thẻ</Text>
							<Text style={styles.text}>Thông báo đẩy,email,SMS</Text>
						</View>
					</TouchableOpacity>
				</View>
				<View style={styles.containerChild}>
					<TouchableOpacity style={styles.button}>
						<Ionicons name={'notifications-outline'} size={25} />
						<View style={styles.containerLabel}>
							<Text style={styles.label}>Lời nhắc</Text>
							<Text style={styles.text}>Thông báo đẩy,email,SMS</Text>
						</View>
					</TouchableOpacity>
				</View>
				<View style={styles.containerChild}>
					<TouchableOpacity style={styles.button}>
						<Ionicons name={'copy-outline'} size={25} />
						<View style={styles.containerLabel}>
							<Text style={styles.label}>Hoạt động khác về bạn</Text>
							<Text style={styles.text}>Thông báo đẩy,email,SMS</Text>
						</View>
					</TouchableOpacity>
				</View>
				<View style={styles.containerChild}>
					<TouchableOpacity style={styles.button}>
						<Ionicons name={'people-outline'} size={25} />
						<View style={styles.containerLabel}>
							<Text style={styles.label}>Cập nhật từ bạn bè</Text>
							<Text style={styles.text}>Thông báo đẩy,email,SMS</Text>
						</View>
					</TouchableOpacity>
				</View>
				<View style={styles.containerChild}>
					<TouchableOpacity style={styles.button}>
						<Ionicons name={'person-add-outline'} size={25} />
						<View style={styles.containerLabel}>
							<Text style={styles.label}>Lời mời kết bạn</Text>
							<Text style={styles.text}>Thông báo đẩy,email,SMS</Text>
						</View>
					</TouchableOpacity>
				</View>
				<View style={styles.containerChild}>
					<TouchableOpacity style={styles.button}>
						<Ionicons name={'person-outline'} size={25} />
						<View style={styles.containerLabel}>
							<Text style={styles.label}>Những người bạn có thể biết</Text>
							<Text style={styles.text}>Thông báo đẩy,email,SMS</Text>
						</View>
					</TouchableOpacity>
				</View>
				<View style={styles.containerChild}>
					<TouchableOpacity style={styles.button}>
						<Ionicons name={'ios-chatbox-outline'} size={25} />
						<View style={styles.containerLabel}>
							<Text style={styles.label}>Sinh nhật</Text>
							<Text style={styles.text}>Thông báo đẩy,email,SMS</Text>
						</View>
					</TouchableOpacity>
				</View>
				<View style={styles.containerChild}>
					<TouchableOpacity style={styles.button}>
						<Ionicons name={'people-outline'} size={25} />
						<View style={styles.containerLabel}>
							<Text style={styles.label}>Nhóm</Text>
							<Text style={styles.text}>Thông báo đẩy,email,SMS</Text>
						</View>
					</TouchableOpacity>
				</View>
				<View style={styles.containerChild}>
					<TouchableOpacity style={styles.button}>
						<Ionicons name={'md-play-circle-outline'} size={25} />
						<View style={styles.containerLabel}>
							<Text style={styles.label}>Video</Text>
							<Text style={styles.text}>Thông báo đẩy,email,SMS</Text>
						</View>
					</TouchableOpacity>
				</View>
				<View style={styles.containerChild}>
					<TouchableOpacity style={styles.button}>
						<Ionicons name={'calendar-outline'} size={25} />
						<View style={styles.containerLabel}>
							<Text style={styles.label}>Sự kiện</Text>
							<Text style={styles.text}>Thông báo đẩy,email,SMS</Text>
						</View>
					</TouchableOpacity>
				</View>
				<View style={styles.containerChild}>
					<TouchableOpacity style={styles.button}>
						<Ionicons name={'flag-outline'} size={25} />
						<View style={styles.containerLabel}>
							<Text style={styles.label}>Trang bạn theo dõi</Text>
							<Text style={styles.text}>Thông báo đẩy,email,SMS</Text>
						</View>
					</TouchableOpacity>
				</View>
				<View style={styles.containerChild}>
					<TouchableOpacity style={styles.button}>
						<Ionicons name={'home-outline'} size={25} />
						<View style={styles.containerLabel}>
							<Text style={styles.label}>Marketplace</Text>
							<Text style={styles.text}>Thông báo đẩy,email,SMS</Text>
						</View>
					</TouchableOpacity>
				</View>
				<View style={styles.containerChild}>
					<TouchableOpacity style={styles.button}>
						<Ionicons name={'heart-circle-outline'} size={25} />
						<View style={styles.containerLabel}>
							<Text style={styles.label}>Chiến dịch gây quỹ và tính huống khẩn cấp</Text>
							<Text style={styles.text}>Thông báo đẩy,email,SMS</Text>
						</View>
					</TouchableOpacity>
				</View>
				<Hr lineColor="#DDD" width={1} text="" />
			</View>
			<View style={styles.container}>
				<Text style={styles.firstText}>Bạn muốn nhận thông báo qua </Text>
				<View style={styles.containerChild}>
					<TouchableOpacity style={styles.button} onPress={() => navigation.navigate('PushNotificationScreen')}>
						<Ionicons name={'ios-chatbox-outline'} size={25} />
						<View style={styles.containerLabel}>
							<Text style={styles.label}>Thông báo đẩy</Text>
							<Text style={styles.text}>Bật</Text>
						</View>
					</TouchableOpacity>
				</View>
				
			</View>


		</ScrollView>

	);

};

const styles = StyleSheet.create({

	container: {
		paddingTop: 10,
		paddingLeft: 15,
		paddingBottom: 15,
		flexDirection: 'column',
		backgroundColor: '#fff',
	},
	firstText: {
		fontWeight: 'bold',
		fontSize: 20,
	},
	containerChild: {
		flexDirection: 'column',
		paddingLeft: 10,
		paddingTop: 15,
	},
	button: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	containerLabel: {
		paddingLeft: 10,
		flexDirection: 'column',
	},
	label: {
		fontWeight: 'bold',
		fontSize: 18,
	},
	text: {

	},


});