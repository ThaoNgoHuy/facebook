import { Form, Input, Item, Label } from 'native-base';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, TextInput, Image, ScrollView } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as colors from './../constants/colors';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import {useRef , useState} from 'react';
import { useNavigation } from '@react-navigation/native';
import Collapsible from 'react-native-collapsible';

export default function ChangePassScreen(){
	
	const [isCollapsed,setIsCollapsed] = useState(true);


	return(
			<View style={styles.container}>
	        	<View style={styles.containerChild}>
					<Text style={styles.header}>Đăng nhập</Text>
					<TouchableOpacity style={styles.button} onPress={ () => setIsCollapsed(!isCollapsed)}>
						<Ionicons name={'person-circle'} size={35} />
						<View style={styles.textChild}>
							<Text style={styles.headerButton}>Đổi mật khẩu</Text>
							<Text style={styles.noticeButton}>Bạn nên sử dụng mật khẩu mạnh mà mình chưa sử dụng ở đâu khác.</Text>
						</View>
					</TouchableOpacity>
					<Collapsible collapsed={isCollapsed}>
	    				<View style={styles.containerCollapse}>
	    					<View style={styles.containerInput}>
		    					<TextInput placeholder='Mật khẩu hiện tại' style={styles.textInput} />
		    					<TextInput placeholder='Mật khẩu mới' style={styles.textInput} />
		    					<TextInput placeholder='Gõ lại mật khẩu mới' style={styles.textInput}  />
		    				</View>
		    				<TouchableOpacity style={styles.buttonBot}>
		    					<Text style={styles.textButton}>Lưu thay đổi </Text>
		    				</TouchableOpacity>
	    				</View>
					</Collapsible>
				</View>
			</View>
	);
};

const styles = StyleSheet.create({
	
	container: {
		backgroundColor : '#DDD',
		flex: 1,
	},
	containerChild: {
		flex: 1,
	},
	header: {
		fontWeight : 'bold',
		fontSize : 20,
		paddingHorizontal : 10,
		paddingTop : 15,
	},
	notice: {
		paddingLeft : 10,
		paddingRight : 10,
	},
	button: {
		flexDirection: 'row',
		paddingTop : 10,
		paddingHorizontal : 10,
	},
	textChild: {
		flexDirection: 'column',
		paddingLeft : 5,
		paddingRight : 20,
	},
	headerButton: {
		fontWeight : 'bold',
		fontSize : 18,
	},
	noticeButton: {
		paddingRight : 10,
	},
	containerCollapse: {
		paddingLeft : 10,
		paddingRight : 10,
		borderRadius: 10,
		flexDirection: 'column',
		justifyContent: 'space-around',
		marginTop : 10,
	},
	containerInput: {
		borderRadius: 10,
		backgroundColor : '#fff',
	},
	textInput: {
		marginBottom : 2,
	},
	buttonBot: {
		backgroundColor: 'blue',
		padding : 10,
		alignItems: 'center',
		marginVertical : 10,
		borderRadius: 5,
	},
	textButton: {
		color: '#fff',
	}
});