import { Form, Input, Item, Label } from 'native-base';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, TextInput, Image, ScrollView, Switch, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as colors from './../constants/colors';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import {useRef , useState} from 'react';
import { useNavigation } from '@react-navigation/native';
import Collapsible from 'react-native-collapsible';
import Hr from "react-native-hr-component";


const windowWidth = Dimensions.get('window').width;


export default function CommentSettingScreen(){

	const [isEnabled, setIsEnabled] = useState(false);
  	const toggleSwitch = () => setIsEnabled(previousState => !previousState);

	return(
		<View style={styles.container}>
			<View style={styles.firstChild}>
				<Text style={styles.notice}>Đây là thông báo khi có người bình luận về bài viết và trả lời bình luận của bạn. Sau đây là ví dụ</Text>
				<View style={styles.example}>
					<Image style={styles.image} source={require('./../../assets/images/avatar.jpg')} />
					<Text style={styles.noticeChild}><Text style={styles.name}>Josephine Williams</Text> đã trả lời một bình luận có gắn thẻ bạn</Text>
				</View>
				<Hr lineColor="#DDD" width={1} text="" />
			</View>
			<View style={styles.secondChild}>
				<Text style={styles.title}>Nơi bạn nhận các thông báo này</Text>
				<View style={styles.config}>
					<Ionicons name={'ios-chatbox-outline'} size={35} />
					<Switch 
						trackColor={{ false: "#767577", true: "#81b0ff" }}
				        thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
				        ios_backgroundColor="#3e3e3e"
				        onValueChange={toggleSwitch}
				        value={isEnabled}
					/>
				</View>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({

	container: {
		backgroundColor: '#fff',
		padding : 15,
	},
	firstChild: {
		paddingTop: 10,
	},
	notice: {
		fontSize: 14,
	},
	example: {
		flexDirection: 'row',
		paddingRight: 10,
		paddingTop: 10,
	},
	image: {
		width : windowWidth/10,
		height: windowWidth/10,
		borderRadius: windowWidth/10,
	},
	noticeChild: {
		paddingLeft: 10,
		paddingRight: 15,
	},
	name: {
		fontWeight: 'bold',
		fontSize: 15
	},
	secondChild: {
		paddingVertical: 15,

	},
	title: {
		fontWeight: 'bold',
		fontSize: 18,
	},
	config: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingTop: 15,
		alignItems: 'center',
	}
});