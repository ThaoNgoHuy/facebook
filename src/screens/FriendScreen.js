import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, TextInput, Image, ScrollView, FlatList, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as colors from './../constants/colors';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import {useRef , useState} from 'react';
import { useLinkProps, useNavigation } from '@react-navigation/native';
import Collapsible from 'react-native-collapsible';
import Hr from "react-native-hr-component";


	const windowWidth = Dimensions.get('window').width;

	const DATA = [
	  {
	    id: "1",
		avatar : './../../assets/images/huyhoang.jpg',
		name: 'Huy Hoang',
		status: true,
	  },
	  {
	    id: "2",
		avatar : './../../assets/images/huyhoang.jpg',
		name: 'Huy Hoang',
		status: false,
	  },
	  {
	    id: "3",
		avatar : './../../assets/images/huyhoang.jpg',
		name: 'Huy Hoang',
		status: true,
	  },
	  {
	    id: "4",
		avatar : './../../assets/images/huyhoang.jpg',
		name: 'Huy Hoang',
		status: false,
	  },
	  {
	    id: "5",
		avatar : './../../assets/images/huyhoang.jpg',
		name: 'Huy Hoang',
		status: true,
	  },
	  {
	    id: "6",
		avatar : './../../assets/images/huyhoang.jpg',
		name: 'Huy Hoang',
		status: true,
	  },
	];

	const headerItem = () => {

		return(

			<View style={styles.headerContainer}>
				<View style={styles.firstChild}>
					<Text style={styles.textTitle}>Friend</Text>
					<TouchableOpacity style={styles.iconButton}>
						<Ionicons name={'ios-search'} size={26} />
					</TouchableOpacity>
				</View>
				<View style={styles.secondChild}>
					<TouchableOpacity style={styles.buttonSug}><Text style={styles.textSug}>Suggestion</Text></TouchableOpacity>
					<TouchableOpacity style={styles.buttonAll}><Text style={styles.textAll}>All Friends</Text></TouchableOpacity>
				</View>
				<Hr lineColor="#DDD" width={1} text="" />
			</View>

		);

	};

	const Item = ({avatar, name, status} ) => {
		if(status){
			return(

				<View style={styles.itemContainer}>
					<Image source={require('./../../assets/images/huyhoang.jpg')} style={styles.itemImage} />
					<View style={styles.itemRight}>
						<Text style={styles.itemName}>{name}</Text>
						<View style={styles.itemButton}>
							<TouchableOpacity style={styles.itemButtonAdd}><Text style={styles.textButton}>Add Friend</Text></TouchableOpacity>
							<TouchableOpacity style={styles.itemButtonDelete}><Text style={styles.textButtonX}>Delete </Text></TouchableOpacity>
						</View>
					</View>
				</View>

			);
		}
		else{
			return(

				<View style={styles.itemContainer}>
					<Image source={require('./../../assets/images/huyhoang.jpg')} style={styles.itemImage} />
					<View style={styles.itemRight}>
						<Text style={styles.itemName}>{name}</Text>
						<TouchableOpacity style={styles.itemButtonCancel}><Text style={styles.textButtonX}>Cancel</Text></TouchableOpacity>
					</View>
				</View>

			);
		}
	}

export default function FriendScreen(){

	const renderItem = ({ item }) => (
		<Item name={item.name} status={item.status}  />
	);

	return(
		<FlatList
        data={DATA}
        renderItem={renderItem}
		keyExtractor={item => item.id}
		ListHeaderComponent={headerItem}
      />
	);
};

const styles = StyleSheet.create({

	headerContainer: {
		flexDirection: 'column',
		paddingHorizontal : 15,
		paddingTop: 10,
	},
	firstChild: {
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	textTitle: {
		fontSize: 20,
		fontWeight: 'bold',
	},
	iconButton: {
		backgroundColor: '#DDD',
		borderRadius: 30,
		paddingVertical : 2,
		paddingHorizontal: 3,
	},
	secondChild: {
		flexDirection: 'row',
	},
	buttonSug: {
		backgroundColor: '#DDD',
		borderRadius: 30,
		paddingVertical : 5,
		paddingHorizontal: 5,
	},
	buttonAll: {
		backgroundColor: '#DDD',
		borderRadius: 30,
		paddingVertical : 5,
		paddingHorizontal: 5,
		marginLeft: 5,
	},
	textSug: {
		fontSize: 15,
		fontWeight: 'bold',
	},
	textAll: {
		fontSize: 15,
		fontWeight: 'bold',
	},

	itemContainer: {
		flexDirection: 'row',
		paddingHorizontal: 15,
		paddingVertical: 10,
		alignItems: 'center',
	},
	itemImage: {
		width: windowWidth/5,
		height: windowWidth/5,
		borderRadius: windowWidth/5,
	},
	itemName: {
		fontSize: 18,
		fontWeight: 'bold',
	},
	itemRight: {
		flexDirection: 'column',
		paddingLeft: 10,
	},
	itemButton: {
		flexDirection: 'row',
		paddingVertical: 2,
	},
	itemButtonAdd: {
		borderRadius: 10,
		paddingVertical: 4,
		paddingHorizontal: 30,
		backgroundColor: '#07A4E8',
	},
	itemButtonDelete:{
		borderRadius: 10,
		marginLeft: 2,
		paddingVertical: 4,
		paddingHorizontal: 30,
		backgroundColor: '#D7E6EE',
	},	
	itemButtonCancel: {
		borderRadius: 10,
		paddingVertical: 4,
		backgroundColor: '#D7E6EE',
		paddingHorizontal: 100,
	},
	textButton: {
		fontSize: 16,
		fontWeight: 'bold',
		color: '#fff',
	},
	textButtonX: {
		fontSize: 16,
		fontWeight: 'bold',
		color: '#020405',
	}

});