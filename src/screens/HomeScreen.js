/* eslint-disable prettier/prettier */
import React from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import PostCard from '../components/PostCard';
import Posts from '../components/Posts';

// const data = {

//   posts: {
//     created: '17 phut',
//     is_liked : true,
//     content: 'abc hello',
//     image:[
//             'https://img.thuthuatphanmem.vn/uploads/2018/10/16/hinh-anh-luffy-de-thuong_101533809.jpg',
//             'https://img.thuthuatphanmem.vn/uploads/2018/10/16/hinh-anh-luffy-de-thuong_101533809.jpg',
//             'https://img.thuthuatphanmem.vn/uploads/2018/10/16/hinh-anh-luffy-de-thuong_101533809.jpg',
//             'https://img.thuthuatphanmem.vn/uploads/2018/10/16/hinh-anh-luffy-de-thuong_101533809.jpg',
//           ],
//     comment: '1 comment',
//   },
//   name: 'Huy Hoang'
// };

const DATA = [

  {
    id: 1,
    data: {
      posts: {
        created: '17 phut',
        is_liked : true,
        content: 'abc hello',
        image:[
                'https://img.thuthuatphanmem.vn/uploads/2018/10/16/hinh-anh-luffy-de-thuong_101533809.jpg',
                'https://img.thuthuatphanmem.vn/uploads/2018/10/16/hinh-anh-luffy-de-thuong_101533809.jpg',
                'https://img.thuthuatphanmem.vn/uploads/2018/10/16/hinh-anh-luffy-de-thuong_101533809.jpg',
                'https://img.thuthuatphanmem.vn/uploads/2018/10/16/hinh-anh-luffy-de-thuong_101533809.jpg',
              ],
        comment: '1 comment',
        video: [],
      },
      name: 'Huy Hoang',
    },
  },

  {
    id: 2,
    data: {
      posts: {
        created: '17 phut',
        is_liked : true,
        content: 'abc hello',
        image:[
                'https://img.thuthuatphanmem.vn/uploads/2018/10/16/hinh-anh-luffy-de-thuong_101533809.jpg',
                
              ],
        comment: '1 comment',
        video: [],
      },
      name: 'Huy Hoang',
      
    },
  },
  {
    id: 3,
    data: {
      posts: {
        created: '17 phut',
        is_liked : true,
        content: 'abc hello',
        image:[
                'https://img.thuthuatphanmem.vn/uploads/2018/10/16/hinh-anh-luffy-de-thuong_101533809.jpg',
                'https://img.thuthuatphanmem.vn/uploads/2018/10/16/hinh-anh-luffy-de-thuong_101533809.jpg',
                
              ],
        comment: '1 comment',
        video: [],
      },
      name: 'Huy Hoang',
    },
  },
  {
    id: 4,
    data: {
      posts: {
        created: '17 phut',
        is_liked : true,
        content: 'abc hello',
        image:[
                'https://img.thuthuatphanmem.vn/uploads/2018/10/16/hinh-anh-luffy-de-thuong_101533809.jpg',
                'https://img.thuthuatphanmem.vn/uploads/2018/10/16/hinh-anh-luffy-de-thuong_101533809.jpg',
                'https://img.thuthuatphanmem.vn/uploads/2018/10/16/hinh-anh-luffy-de-thuong_101533809.jpg',                
              ],
        video: [],
        comment: '1 comment',
      },
      name: 'Huy Hoang',
    },
  },
  {
    id: 5,
    data: {
      posts: {
        created: '17 phut',
        is_liked : true,
        content: 'abc hello',
        image:[],
        comment: '1 comment',
        video: ['https://www.youtube.com/watch?v=lTFmLpRJkII&list=RDMMl2UiY2wivTs&index=3'],
      },
      name: 'Huy Hoang',
    },
  },

];

function HomeScreen() {

  const renderItem = ({ item }) => {
    return(
      <Posts data={item.data} />
    );
  };

  return (

    <View style={styles.container}>
      <FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        ListHeaderComponent={PostCard}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default HomeScreen;
