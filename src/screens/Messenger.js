import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, TextInput, Image, ScrollView, FlatList, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as colors from '../constants/colors';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import {useRef , useState} from 'react';
import { useLinkProps, useNavigation } from '@react-navigation/native';
import Collapsible from 'react-native-collapsible';
import Hr from "react-native-hr-component";
import Message from '../components/Message';


const DATA = [
    {
        id: '1',
        avatar: 'https://scontent.fhan5-5.fna.fbcdn.net/v/t1.0-9/109851811_1418394418355331_6565440229683774898_n.jpg?_nc_cat=101&ccb=2&_nc_sid=09cbfe&_nc_ohc=w_jMreNRCLEAX8qJti5&_nc_ht=scontent.fhan5-5.fna&oh=e794815f1eb8b78135085755d7381e16&oe=5FD1E0DB',
        name: 'Huy Hoang',
        description: 'abcdefghjklaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    },
    {
        id: '2',
        avatar: 'https://scontent.fhan5-5.fna.fbcdn.net/v/t1.0-9/29684097_167155184002956_8798470901333980676_n.jpg?_nc_cat=108&ccb=2&_nc_sid=09cbfe&_nc_ohc=9r8kfuDryTwAX-cb4Ga&_nc_ht=scontent.fhan5-5.fna&oh=1cd97c8cfefd296bd53c2e30fa4278cf&oe=5FD2E234',
        name: 'Van Trang',
        description: 'abcdefghjklaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    },
    {
        id: '3',
        avatar: 'https://scontent.fhan5-7.fna.fbcdn.net/v/t1.0-9/58384051_956619994508746_7484001902217134080_n.jpg?_nc_cat=103&ccb=2&_nc_sid=174925&_nc_ohc=7R-9XfuIZb4AX-P2cwg&_nc_ht=scontent.fhan5-7.fna&oh=acea87f6b40bc247132de39239bd0685&oe=5FD23FB7',
        name: 'Van Son',
        description: 'abcdefghjklaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    },
    {
        id: '4',
        avatar: 'https://scontent.fhan5-5.fna.fbcdn.net/v/t1.0-9/119119857_2754107754867066_7437387580383446260_o.jpg?_nc_cat=101&ccb=2&_nc_sid=09cbfe&_nc_ohc=6vKEozIa3IwAX_5ulUr&_nc_ht=scontent.fhan5-5.fna&oh=bbfd487a17a0e09a8addadc35fa95aa7&oe=5FD276D4',
        name: 'Gia Bao',
        description: 'abcdefghjklaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    },
    {
        id: '5',
        avatar: 'https://scontent.fhan5-3.fna.fbcdn.net/v/t1.0-9/82911732_2469226896634060_5759876401809326080_o.jpg?_nc_cat=111&ccb=2&_nc_sid=09cbfe&_nc_ohc=kUanMLCKanYAX_6vuZx&_nc_ht=scontent.fhan5-3.fna&oh=7eb19dfd4ad5065b5cf44ba5cb1c2c37&oe=5FD2B9E2',
        name: 'Quang Linh',
        description: 'abcdefghjklaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    },
    {
        id: '6',
        avatar: 'https://scontent.fhan5-5.fna.fbcdn.net/v/t1.0-9/22528147_1956454591292922_2997640443589196452_n.jpg?_nc_cat=108&ccb=2&_nc_sid=09cbfe&_nc_ohc=1NwrATwGYL0AX9ECz-T&_nc_ht=scontent.fhan5-5.fna&oh=43dc8288b3bd2cce033c5cc37a885bbd&oe=5FD0569D',
        name: 'Huu Giang',
        description: 'abcdefghjklaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    },
    {
        id: '7',
        avatar: 'https://scontent.fhan5-6.fna.fbcdn.net/v/t1.0-9/122133918_1205421646504019_8841926392515335496_n.jpg?_nc_cat=105&ccb=2&_nc_sid=09cbfe&_nc_ohc=GwcNzZPwlQQAX--pssr&_nc_ht=scontent.fhan5-6.fna&oh=e733621fee65c21e15917471508668bd&oe=5FD2B54B',
        name: 'Nhung Bui',
        description: 'abcdefghjklaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    },
    {
        id: '8',
        avatar: 'https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/117747228_1653384871492872_1099033318758005333_o.jpg?_nc_cat=109&ccb=2&_nc_sid=8bfeb9&_nc_ohc=d7LHvJ9WEBUAX9u7XWC&_nc_ht=scontent.fhan5-1.fna&oh=d9803d5b4353dedfa1bcd34c93733f9c&oe=5FD2509E',
        name: 'Uyen Thuy',
        description: 'abcdefghjklaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    },
    {
        id: '9',
        avatar: 'https://scontent.fhan5-6.fna.fbcdn.net/v/t1.0-9/70534535_2413827008861330_1715928087171956736_o.jpg?_nc_cat=107&ccb=2&_nc_sid=a4a2d7&_nc_ohc=mGvHiYxk9PoAX-5ZE2i&_nc_ht=scontent.fhan5-6.fna&oh=003f55ccc11594c7658058883994e215&oe=5FD19559',
        name: 'Thi Thao',
        description: 'abcdefghjklaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    },
    {
        id: '10',
        avatar: 'https://scontent.fhan5-6.fna.fbcdn.net/v/t1.0-9/106218983_146354477050838_2369273070899105191_n.jpg?_nc_cat=107&ccb=2&_nc_sid=8bfeb9&_nc_ohc=viNdOQe5fY4AX9FFAI3&_nc_ht=scontent.fhan5-6.fna&oh=843e1c9c6360cb089b7a1a685e8d726c&oe=5FD19164',
        name: 'Huyen Trang',
        description: 'abcdefghjklaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    },
    {
        id: '11',
        avatar: 'https://scontent.fhan5-3.fna.fbcdn.net/v/t1.0-9/121778738_838288213664757_8616650405010886511_o.jpg?_nc_cat=106&ccb=2&_nc_sid=8bfeb9&_nc_ohc=aZ-3fCehJRsAX8LO9v5&_nc_ht=scontent.fhan5-3.fna&oh=79a565bdef9a77a8c8ffc8629fc010b8&oe=5FD06024',
        name: 'Bich Ngoc',
        description: 'abcdefghjklaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    },

];

export default function Messenger(){
    
    const renderItem = ({ item }) => {
        return(
          <Message item={item} />
        );
    };

    return(

        <View style={styles.container}>
            <FlatList
                data={DATA}
                renderItem={renderItem}
                keyExtractor={item => item.id}
            />
        </View>
        
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
});