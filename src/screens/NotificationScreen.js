/* eslint-disable prettier/prettier */
import React from 'react';
import { Text, View,FlatList } from 'react-native';
import Notification from './../components/Notification';

const DATA = [

	{
		id : '1',
		avatar : './../../assets/images/huyhoang.jpg',
		content : 'Huy Hoàng đã thích ảnh của bạn',
		time : '15 phút trước',
	},
	{
		id : '2',
		avatar : './../../assets/images/huyhoang.jpg',
		content : 'Huy Hoàng đã nhắc đến bạn trong một bình luận',
		time : '15 phút trước',
	},
	{
		id : '3',
		avatar : './../../assets/images/huyhoang.jpg',
		content : 'Huy Hoàng đã nhắc đến bạn trong một bình luận',
		time : '15 phút trước',
	},
	{
		id : '4',
		avatar : './../../assets/images/huyhoang.jpg',
		content : 'Huy Hoàng đã nhắc đến bạn trong một bình luận',
		time : '15 phút trước',
	},
	{
		id : '5',
		avatar : './../../assets/images/huyhoang.jpg',
		content : 'Huy Hoàng đã nhắc đến bạn trong một bình luận',
		time : '15 phút trước',
	},
	{
		id : '6',
		avatar : './../../assets/images/huyhoang.jpg',
		content : 'Huy Hoàng đã thích một hoạt động của bạn',
		time : '15 phút trước',
	},
	{
		id : '7',
		avatar : './../../assets/images/huyhoang.jpg',
		content : 'Huy Hoàng đã phát trực tiếp',
		time : '15 phút trước',
	},
	{
		id : '8',
		avatar : './../../assets/images/huyhoang.jpg',
		content : 'Huy Hoàng đã nhắc đến bạn trong một bình luận',
		time : '15 phút trước',
	},
	{
		id : '9',
		avatar : './../../assets/images/huyhoang.jpg',
		content : 'Huy Hoàng đã nhắc đến bạn trong một bình luận',
		time : '15 phút trước',
	},
	{
		id : '10',
		avatar : './../../assets/images/huyhoang.jpg',
		content : 'Huy Hoàng đã nhắc đến bạn trong một bình luận',
		time : '15 phút trước',
	},
];


function NotificationScreen() {

	const renderItem = ({ item }) => (
    	<Notification content={item.content} time={item.time} />
  );

  return (
    <View>
    	<FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        initialNumToRender={6}
      />
    </View>
  );
}

export default NotificationScreen;
