import { Form, Input, Item, Label } from 'native-base';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, TextInput, Image, ScrollView, Switch, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as colors from './../constants/colors';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import {useRef , useState} from 'react';
import { useNavigation } from '@react-navigation/native';
import Collapsible from 'react-native-collapsible';
import Hr from "react-native-hr-component";
import SoundPlayer from 'react-native-sound-player';


export default function PushNotificationScreen(){

	const [isEnabled_1, setIsEnabled_1] = useState(false);
  	const toggleSwitch_1 = () =>{
  		setIsEnabled_1(!isEnabled_1);
  	}

  	const [isEnabled_2, setIsEnabled_2] = useState(false);
  	const toggleSwitch_2 = () =>{
  		setIsEnabled_2(!isEnabled_2);  	
  	}

  	const [isEnabled_3, setIsEnabled_3] = useState(false);
  	const toggleSwitch_3 = () =>{
  		setIsEnabled_3(!isEnabled_3);  	
  	}

  	
	return(

		<View style={styles.container}>
			<TouchableOpacity style={styles.child} onPress={() => toggleSwitch_1()}>
				<View style={styles.headChild}>
					<Ionicons name={'notifications-off'} size={30} />
					<View style={styles.mid}>
						<Text style={styles.title}>Tắt thông báo đẩy</Text>
						<Text style={styles.text}>tắt</Text>
					</View>
				</View>
				<Switch 
					trackColor={{ false: "#767577", true: "#81b0ff" }}
			        thumbColor={isEnabled_1 ? "#f5dd4b" : "#f4f3f4"}
			        ios_backgroundColor="#3e3e3e"
			        value={isEnabled_1}
			        onValueChange={toggleSwitch_1}
				/>
			</TouchableOpacity>
			<TouchableOpacity style={styles.child} onPress={toggleSwitch_2}>
				<View style={styles.headChild}>
					<Ionicons name={'md-flash-sharp'} size={30} />
					<View style={styles.mid}>
						<Text style={styles.title}>Đèn LED điện thoại</Text>
						<Text style={styles.text}>Nhấp nháy đèn LED khi có thông báo đến</Text>
					</View>
				</View>
				<Switch 
					trackColor={{ false: "#767577", true: "#81b0ff" }}
			        thumbColor={isEnabled_2 ? "#f5dd4b" : "#f4f3f4"}
			        ios_backgroundColor="#3e3e3e"
			        value={isEnabled_2}
			        onValueChange={toggleSwitch_2}
				/>
			</TouchableOpacity>
			<TouchableOpacity style={styles.child} onPress={ () => { toggleSwitch_3()}}>
				<View style={styles.headChild}>
					<Ionicons name={'volume-medium'} size={30} />
					<View style={styles.mid}>
						<Text style={styles.title}>Âm thanh</Text>
						<Text style={styles.text}>Phát âm thanh khi có thông báo đến</Text>
					</View>
				</View>
				<Switch 
					trackColor={{ false: "#767577", true: "#81b0ff" }}
			        thumbColor={isEnabled_3 ? "#f5dd4b" : "#f4f3f4"}
			        ios_backgroundColor="#3e3e3e"
			        value={isEnabled_3}
			        onValueChange={toggleSwitch_3}
				/>
			</TouchableOpacity>
		</View>
		
	);

};

const styles=StyleSheet.create({

	container: {
		backgroundColor: '#fff',
	},
	child: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingVertical: 15,
		paddingLeft: 15,
		paddingRight: 15,
	},
	headChild: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	mid: {
		paddingLeft: 15,
	},
	title: {
		fontSize: 16,
		fontWeight: 'bold',
	},
	text: {
		fontSize: 12,
	},

});