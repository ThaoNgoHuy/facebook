import { Form, Input, Item, Label } from 'native-base';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, TextInput, Image, ScrollView } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as colors from './../constants/colors';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import {useRef , useState} from 'react';
import { useNavigation } from '@react-navigation/native';


export default function SettingAccountScreen(){

	const navigation = useNavigation();


	return(

		<ScrollView>
			<View style={styles.container}>
				<View styles={styles.containerChild} >
					<Text style={styles.header}>Cài đặt tài khoản</Text>
					<Text style={styles.notice}>Quản lý thông tin về bạn, các khoản thanh toán và danh bạn của bạn cũng như tài khoản nói chung.</Text>
					<TouchableOpacity style={styles.button} onPress={() => navigation.navigate('YourInfoSetting')}>
						<Ionicons name={'person-circle'} size={35} />
						<View style={styles.textChild}>
							<Text style={styles.headerButton}>Thông tin cá nhân</Text>
							<Text style={styles.noticeButton}>Cập nhật tên, số điện thoại và địa chỉ email của bạn.</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>
			<View style={styles.container}>
				<View styles={styles.containerChild} >
					<Text style={styles.header}>Bảo mật</Text>
					<Text style={styles.notice}>Đổi mật khẩu và thực hiện các hành động khác để tăng cường bảo mật cho tài khoản của bạn.</Text>
					<TouchableOpacity style={styles.button} onPress={ () => navigation.navigate('ChangePassScreen')}>
						<Ionicons name={'shield-checkmark'} size={35} />
						<View style={styles.textChild}>
							<Text style={styles.headerButton}>Bảo mật và đăng nhập</Text>
							<Text style={styles.noticeButton}>Đổi mật khẩu và thực hiện các hành động khác để tăng cường bảo mật cho tài khoản của bạn.</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>
			<View style={styles.container}>
				<View styles={styles.containerChild} >
					<Text style={styles.header}>Quyền riêng tư</Text>
					<Text style={styles.notice}>Kiểm soát người nhìn thấy hoạt động của bạn trên Facebook và cách chúng tôi dùng dữ liệu để các nhân hóa trải nghiệm.</Text>
					<TouchableOpacity style={styles.button}>
						<Ionicons name={'md-remove-circle-sharp'} size={35} />
						<View style={styles.textChild}>
							<Text style={styles.headerButton}>Chặn</Text>
							<Text style={styles.noticeButton}>Xem lại những người bạn đã chặn trước đó.</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>
			<View style={styles.container}>
				<View styles={styles.containerChild} >
					<Text style={styles.header}>Thông báo</Text>
					<Text style={styles.notice}>Quyết định cách bạn muốn giao tiếp trên Facebook và chúng tôi nên liên hệ với bạn bằng cách nào.</Text>
					<TouchableOpacity style={styles.button} onPress={() => navigation.navigate('AnnounSettingScreen')}>
						<Ionicons name={'notifications-outline'} size={35} />
						<View style={styles.textChild}>
							<Text style={styles.headerButton}>Cài đặt thông báo</Text>
							<Text style={styles.noticeButton}>Chọn thông báo bạn muốn nhận và nơi nhận thông báo</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>
		</ScrollView>
	);
};

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#fff',
		marginBottom : 10,
		paddingBottom : 10,
	},
	containerChild: {
		backgroundColor : 'red',
		borderWidth: 1,
	},
	header: {
		fontWeight : 'bold',
		fontSize : 20,
		paddingHorizontal : 10,
		paddingTop : 15,
	},
	notice: {
		paddingLeft : 10,
		paddingRight : 10,
	},
	button: {
		flexDirection: 'row',
		paddingTop : 10,
		paddingHorizontal : 10,
	},
	textChild: {
		flexDirection: 'column',
		paddingLeft : 5,
		paddingRight : 20,
	},
	headerButton: {
		fontWeight : 'bold',
		fontSize : 18,
	},
	noticeButton: {
		paddingRight : 10,
	},
});