/* eslint-disable prettier/prettier */
import React from 'react';
import { Text, View, ScrollView, TouchableOpacity, Image, StyleSheet, Dimensions, BackHandler } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import  {Component , useState} from 'react';
import Collapsible from 'react-native-collapsible';
import RNExitApp from 'react-native-exit-app';
import { useNavigation } from '@react-navigation/native';


const windowWidth = Dimensions.get('window').width;


function SettingScreen() {

	const navigation = useNavigation();
	const [iconName1, setIconName1] = useState('chevron-down');
	const [iconName2, setIconName2] = useState('chevron-down');

	const [isCollapsed1,setIsCollapsed1] = useState(true);
	const [isCollapsed2,setIsCollapsed2] = useState(true);

  return (
  		<ScrollView style={styles.container}>
	    	<View style={styles.topTab}>
	    		<TouchableOpacity style={styles.menuButton}>
	    			<Text style={styles.menu}>Menu</Text>
	    		</TouchableOpacity>
	    		<TouchableOpacity style={styles.searchButton}>
	    			<Ionicons name={'search-sharp'} size={28} />
	    		</TouchableOpacity>
	    	</View>
	    	<View style={styles.midTab}>
	    		<TouchableOpacity style={styles.user}>
	    			<Image source={require('./../../assets/images/avatar.jpg')} style={styles.avatar} />
	    			<View style={styles.info}>
	    				<Text style={styles.name}>Huy Thao</Text>
	    				<Text style={styles.notice}>Xem trang cá nhân của bạn</Text>
	    			</View>
	    		</TouchableOpacity>
	    	</View>
	    	<View style={styles.bottomTab}>
	    		<View style={styles.bottomChild}>
	    			<TouchableOpacity style={styles.bottomButton} onPress={() => { iconName1 === 'chevron-down' ? setIconName1('chevron-up') : setIconName1('chevron-down'); setIsCollapsed1(!isCollapsed1) }}>
	    				<View style={styles.contentButton}>
	    					<Ionicons name={'help-circle'} size={25} />
	    					<Text style={styles.textChild}>Trợ giúp & hỗ trợ</Text>
	    				</View>
	    				<View style={styles.rightIconBottom}>
	    					<Ionicons size={25} name={iconName1} />
	    				</View>
	    			</TouchableOpacity>
	    			<Collapsible collapsed={isCollapsed1}>
	    				<TouchableOpacity style={styles.bottomButtonCollapse} >
		    				<View style={styles.contentButton}>
		    					<Ionicons name={'browsers-outline'} size={25} />
		    					<Text style={styles.textChild}>Điều khoản và chính sách</Text>
		    				</View>
	    				</TouchableOpacity>
					</Collapsible>
	    		</View>
	    		<View style={styles.bottomChild}>
	    			<TouchableOpacity style={styles.bottomButton} onPress={() => { iconName2 === 'chevron-down' ? setIconName2('chevron-up') : setIconName2('chevron-down'); setIsCollapsed2(!isCollapsed2) }}>
	    				<View style={styles.contentButton}>
	    					<Ionicons name={'settings-outline'} size={25} />
	    					<Text style={styles.textChild}>Cài đặt & quyền riêng tư</Text>
	    				</View>
	    				<View style={styles.rightIconBottom}>
	    					<Ionicons size={25} name={iconName2} />
	    				</View>
	    			</TouchableOpacity>
	    			<Collapsible collapsed={isCollapsed2}>
	    				<TouchableOpacity style={styles.bottomButtonCollapse} onPress={() => navigation.navigate('SettingAccountScreen')} >
		    				<View style={styles.contentButton}>
		    					<Ionicons name={'person-circle-outline'} size={25} />
		    					<Text style={styles.textChild}>Cài đặt</Text>
		    				</View>
	    				</TouchableOpacity>
					</Collapsible>
	    		</View>
	    		<View style={styles.bottomChild}>
	    			<TouchableOpacity style={styles.bottomButton} >
	    				<View style={styles.contentButton}>
	    					<Ionicons name={'log-out-outline'} size={25} />
	    					<Text style={styles.textChild}>Đăng xuất</Text>
	    				</View>
	    			</TouchableOpacity>
	    		</View>
	    		<View style={styles.bottomChild}>
	    			<TouchableOpacity style={styles.bottomButton} onPress={() => {BackHandler.exitApp();}} >
	    				<View style={styles.contentButton}>
	    					<Ionicons name={'exit'} size={25} />
	    					<Text style={styles.textChild}>Thoát</Text>
	    				</View>
	    			</TouchableOpacity>
	    		</View>
	    	</View>
	    </ScrollView>
    
  );
}

export default SettingScreen;

const styles = StyleSheet.create({
	container: {
		backgroundColor: "#DDDDDD",
	},
	topTab: {
		padding : 15,
		flexDirection : 'row',
		justifyContent: 'space-between',
	},
	menuButton: {
	},
	menu: {
		fontWeight: 'bold',
		fontSize: 20,
	},
	searchButton: {
	},
	midTab: {
		flexDirection: 'column',
		paddingLeft : 15,
	},
	user: {
		flexDirection: 'row',
		alignItems: 'stretch',
	},
	avatar: {
		width : windowWidth/8,
		height : windowWidth/8,
		borderRadius : windowWidth/8
	},
	info: {
		flexDirection : 'column',
		paddingLeft : 10,
	},
	name: {
		fontWeight: 'bold',
		fontSize: 18,
	},
	notice: {
		fontSize : 15,
	},
	bottomTab: {
		flexDirection: 'column',
	    backgroundColor: "#DDDDDD",
	    marginTop : 20,
	    flex : 1,
	},
	bottomChild: {
		padding : 10,

	},
	bottomButton: {
		flexDirection: 'row',
	    borderRadius: 10,
	    paddingVertical : 5,
	    justifyContent: 'space-between',
	    paddingHorizontal : 5,
	},
	bottomButtonCollapse: {
		flexDirection: 'row',
	    borderRadius: 10,
	    paddingVertical : 10,
	    justifyContent: 'space-between',
	    backgroundColor: '#fff',
	    paddingHorizontal: 5,
	},
	contentButton: {
		flexDirection: 'row',
	},
	textChild: {
		fontSize: 17,
		fontWeight: 'bold',
		paddingLeft: 10,
	},
	rightIconBottom: {
		paddingRight : 10,
	}

});
