import { Form, Input, Item, Label } from 'native-base';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, TextInput, Image, ScrollView } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as colors from './../constants/colors';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import {useRef , useState} from 'react';
import ImagePicker from 'react-native-image-picker';


export default function StatusScreen(){

	const panelRef = useRef(null);
	const options = {
		title : 'select image',
		takePhotoButtonTitle : 'Take photo with your camera',
		chooseFromLibraryButtonTitle : 'Choose photo from Library',

	};
	const [isInput, setIsInput] = useState(false);
	const [resource,setResource] = useState([]);
	selectOptions = () => {
		ImagePicker.showImagePicker(options,(response)=>{
			if(response.didCancel){
				console.log('abc');
			}
			else if(response.error){
				console.log(response.error);
			}
			else{
				let source = response.uri;
				setResource( resource => [...resource, source]);
			}
		})
	};

	return(
		<>
			<ScrollView style = {styles.container}>
				<View style={styles.firstChild}>
					<View>
						<Image source={require('./../../assets/images/avatar.jpg')} style={styles.avatar} ></Image>
					</View>
					<View style={styles.columnSecond}>
						<Text style={styles.nameUser}>Huu Giang</Text>
						<View style={styles.optional}>
							<TouchableOpacity style={styles.buttonTouch}>
				              <Ionicons name={'people-sharp'} color={colors.red600} size={14} style={styles.icon}/>
				              <Text> Ban be</Text>
				              <Ionicons name={'caret-down-outline'} color={colors.red600} size={14} style={styles.icon}/>
							</TouchableOpacity>
							<TouchableOpacity style={styles.buttonTouch}>
				              <Ionicons name={'add'} color={colors.red600} size={14} style={styles.icon} />
				              <Text>Album</Text>
				              <Ionicons name={'caret-down-outline'} color={colors.red600} size={14} style={styles.icon} />
							</TouchableOpacity>
						</View>
					</View>
				</View>
				<View>
					<TextInput placeholder="Ban dang nghi gi" multiline={true} style={styles.textInput} onFocus={() =>{panelRef.current.snapTo(2); setIsInput(true)} } />
				</View>
				<View>
					<Text><</Text>
				</View>
			</ScrollView>
			<BottomSheet
					
					snapPoints={[270, 200, 35]}
	        		borderRadius={10}
	        		ref={panelRef}
					renderContent={() => {return(
						
						<ScrollView>
							<View style={styles.containerBottomSheet}>
								<TouchableOpacity style={styles.buttonBottomSheet} onPress={selectOptions}>
									<Ionicons name={'image-outline'} size={24} />
									<Text> Ảnh/Video</Text>
								</TouchableOpacity>
							</View>
							<View style={styles.containerBottomSheet}>
								<TouchableOpacity style={styles.buttonBottomSheet}>
									<Ionicons name={'ios-people-outline'} size={24} />
									<Text> Bạn bè</Text>
								</TouchableOpacity>
							</View>
							<View style={styles.containerBottomSheet}>
								<TouchableOpacity style={styles.buttonBottomSheet}>
									<Ionicons name={'happy-outline'} size={24} />
									<Text> Cảm xúc/Hoạt động</Text>
								</TouchableOpacity>
							</View>
							<View style={styles.containerBottomSheet}>
								<TouchableOpacity style={styles.buttonBottomSheet}>
									<Ionicons name={'location-outline'} size={24} />
									<Text> Check in</Text>
								</TouchableOpacity>
							</View>
							<View style={styles.containerBottomSheet}>
								<TouchableOpacity style={styles.buttonBottomSheet}>
									<Ionicons name={'ios-images-outline'} size={24} />
									<Text> Ảnh 3D</Text>
								</TouchableOpacity>
							</View>
						</ScrollView>
					)}}
					renderHeader={()=> {
						if(isInput){
							return(
								<View style={styles.headerBottomSheet}>
									<View style={styles.textHeaderBottom}>
										<Text style={{fontSize : 18}}>Thêm vào bài viết của bạn</Text>
									</View>
									<View style={styles.iconHeaderBottom}>
										<TouchableOpacity>
											<Ionicons name={'image-outline'} size={25} />
										</TouchableOpacity>
										<TouchableOpacity>
											<Ionicons name={'ios-people-outline'} size={25} />
										</TouchableOpacity>
										<TouchableOpacity>
											<Ionicons name={'happy-outline'} size={25} />
										</TouchableOpacity>
										<TouchableOpacity>
											<Ionicons name={'location-outline'} size={25} />
										</TouchableOpacity>
									</View>
								</View>
							);
						}
						else{
							return(
								<View style={{alignItems : 'center', backgroundColor : '#fff'}}>
									<TouchableOpacity>
										<Ionicons name={'remove'} size={30} />
									</TouchableOpacity>
								</View>
							);
						}
					}}
			>		
			</BottomSheet>
		</>
	);
};

const styles = StyleSheet.create({

	container : {
		flex : 1,
		flexDirection : "column",
		paddingLeft : 10,
		paddingTop : 25,
		paddingRight : 10,
	},
	firstChild : {
		flexDirection : "row",
	},
	avatar : {
		borderRadius : 50,
		width : 50,
		height : 50,
	},
	nameUser : {
		fontWeight : "bold",
		fontSize : 16,
	},
	columnSecond : {
		flexDirection : "column",
		paddingLeft : 15,
	}, 
	buttonTouch: {
		flexDirection : "row",
		paddingRight : 5,
		justifyContent : 'center',
		backgroundColor : '#fff',
		marginRight : 5,
		borderRadius : 2,
		alignItems : 'center',
	},
	
	optional : {
		flexDirection : "row",
		paddingTop : 10,
	},
	icon : {
		paddingLeft : 1,
	},

	textInput : {
		fontSize : 20,
	},
	headerBottomSheet : {
		flexDirection : "row",
		flex : 1,
		padding : 5,
		backgroundColor : '#fff',
		justifyContent : 'space-between',
	},
	textHeaderBottom: {
		
	},
	iconHeaderBottom : {
		flexDirection : "row",
	},
	containerBottomSheet : {
		flexDirection : "column",
		padding : 10,
		backgroundColor : '#fff',
	},
	buttonBottomSheet : {
		alignItems : 'stretch',
		flexDirection : 'row',
		alignItems : 'center',
	}
});