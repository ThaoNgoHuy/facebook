import { Form, Input, Item, Label } from 'native-base';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, TextInput, Image, ScrollView } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as colors from './../constants/colors';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import {useRef , useState} from 'react';
import { useNavigation } from '@react-navigation/native';
import Collapsible from 'react-native-collapsible';

export default function YourInfoSetting(){

	const [isCollapsed,setIsCollapsed] = useState(true);

	return(

			<View style={styles.container}>
	        	<View style={styles.containerChild}>
					<Text style={styles.header}>Thông tin cá nhân</Text>
					<TouchableOpacity style={styles.button} onPress={ () => setIsCollapsed(!isCollapsed)}>
						<Ionicons name={'person-circle'} size={40} />
						<View style={styles.textChild}>
							<Text style={styles.headerButton}>Tên</Text>
							<Text style={styles.noticeButton}>Huy Hoàng</Text>
						</View>
					</TouchableOpacity>
					<Collapsible collapsed={isCollapsed}>
	    				<View style={styles.containerCollapse}>
	    					<View style={styles.containerInput}>
	    						<Text>Họ</Text>
		    					<TextInput placeholder='Họ của bạn' style={styles.textInput} />
		    					<Text>Tên đệm</Text>
		    					<TextInput placeholder='Tên đệm của bạn' style={styles.textInput} />
		    					<Text>Tên</Text>
		    					<TextInput placeholder='Tên của bạn' style={styles.textInput}  />
		    					<View style={styles.alert}>
		    						<Text><Text style={{fontWeight: 'bold'}}>Xin lưu ý rằng: </Text>Nếu đổi tên trên Facebook, bạn không thể đổi lại tên trong 60 ngày. Đừng thêm bất cứ cách viết hoa khác thường, dấu câu, ký tự hoặc các từ ngẫu nhiên.</Text>
		    					</View>
		    				</View>
		    				<TouchableOpacity style={styles.buttonBot}>
		    					<Text style={styles.textButton}>Lưu thay đổi </Text>
		    				</TouchableOpacity>
	    				</View>
					</Collapsible>
				</View>
			</View>
	);

};
const styles = StyleSheet.create({
	
	container: {
		backgroundColor : '#DDD',
		flex: 1,
	},
	containerChild: {
		flex: 1,
	},
	header: {
		fontWeight : 'bold',
		fontSize : 20,
		paddingHorizontal : 10,
		paddingTop : 15,
	},
	notice: {
		paddingLeft : 10,
		paddingRight : 10,
	},
	button: {
		flexDirection: 'row',
		paddingTop : 10,
		paddingHorizontal : 10,
	},
	textChild: {
		flexDirection: 'column',
		paddingLeft : 5,
		paddingRight : 20,
	},
	headerButton: {
		fontWeight : 'bold',
		fontSize : 18,
	},
	noticeButton: {
		paddingRight : 10,
	},
	containerCollapse: {
		paddingLeft : 10,
		paddingRight : 10,
		borderRadius: 10,
		marginTop : 10,
	},
	containerInput: {
		borderRadius: 10,
		backgroundColor : '#fff',
		paddingHorizontal: 10,
		paddingVertical : 10,
	},
	textInput: {
		marginBottom : 2,
	},
	buttonBot: {
		backgroundColor: 'blue',
		padding : 10,
		alignItems: 'center',
		marginVertical : 10,
		borderRadius: 5,
	},
	textButton: {
		color: '#fff',
	},
	alert: {
		borderWidth : 1,
		padding : 5,
	}
});