/* eslint-disable prettier/prettier */
import { Form, Input, Item, Label } from 'native-base';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import * as colors from './../../constants/colors';

function NameScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.text}>Bạn tên gì?</Text>
      </View>
      <View style={styles.mid}>
        <Form style={styles.form}>
          <Item floatingLabel style={styles.item}>
            <Label style={styles.label}>Họ</Label>
            <Input
              underlineColorAndroid={colors.blueA400}
              selectionColor={colors.blue800}
            />
          </Item>
          <Item floatingLabel style={styles.item}>
            <Label style={styles.label}>Tên</Label>
            <Input
              underlineColorAndroid={colors.blueA400}
              selectionColor={colors.blue800}
            />
          </Item>
        </Form>
      </View>
      <View style={styles.topView}>
          <TouchableOpacity
            
          >
            <View style={styles.topButton}>
              <Text style={styles.topButtonText}>Tiếp</Text>
            </View>
          </TouchableOpacity>
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: colors.white,
    flex: 1,
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 48,
  },
  text: {
    color: colors.grey900,
    fontSize: 18,
    fontWeight: 'bold',
  },
  mid: {
    flexDirection: 'row',
  },
  form: {
    flexDirection: 'row',
    flexGrow: 1,
    color: 'red',
  },
  item: {
    flexGrow: 1,
  },
  label: {
    color: colors.blue800,
    fontSize: 12,
  },
  bottom: {
    backgroundColor: 'red'
  },
  topView: {
    alignSelf: 'stretch',
    marginTop: 32,
  },
  topButton: {
    alignItems: 'center',
    backgroundColor: colors.blueA400,
    borderRadius: 8,
    padding: 12,
  },
  topButtonText: {
    color: colors.white,
  },
});

export default NameScreen;
