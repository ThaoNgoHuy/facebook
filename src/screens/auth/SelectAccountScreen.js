/* eslint-disable prettier/prettier */
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import * as colors from './../../constants/colors';

function SelectAccountScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <Text>top</Text>
      </View>
      <View style={styles.bottom}>
        <TouchableOpacity
          onPress={() => navigation.navigate('CreateAccountScreen')}
        >
          <View style={styles.bottomView}>
            <Text style={styles.bottomText}>TẠO TÀI KHOẢN FACEBOOK MỚI</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
    justifyContent: 'flex-end',
  },

  top: {
    flexGrow: 1,
    backgroundColor: 'red',
  },

  bottom: {
    paddingBottom: 24,
    paddingLeft: 24,
    paddingRight: 24,
  },
  bottomView: {
    backgroundColor: colors.blue50,
    borderRadius: 8,
    padding: 8,
  },
  bottomText: {
    color: colors.blue800,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default SelectAccountScreen;
