/* eslint-disable prettier/prettier */
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import CreateAccountScreen from '../CreateAccountScreen';
import NameScreen from '../NameScreen';
import SelectAccountScreen from './../SelectAccountScreen';

const Stack = createStackNavigator();

function AuthStackNavigator() {
  return (
    <Stack.Navigator initialRouteName="SelectAccountScreen">
      <Stack.Screen
        name="SelectAccountScreen"
        component={SelectAccountScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="CreateAccountScreen"
        component={CreateAccountScreen}
        options={{
          title: 'Tạo tài khoản',
        }}
      />
      <Stack.Screen
        name="NameScreen"
        component={NameScreen}
        options={{
          title: 'Tên',
        }}
      />
    </Stack.Navigator>
  );
}

export default AuthStackNavigator;
