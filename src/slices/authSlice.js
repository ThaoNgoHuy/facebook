/* eslint-disable prettier/prettier */
import { createSlice } from '@reduxjs/toolkit';

const auth = createSlice({
  name: 'auth',
  initialState: {
    isLoading: true,
    userToken: null,
  },
  reducers: {
    restoreToken: (state, action) => {
      state.isLoading = false;
      state.userToken = action.payload.token;
    },
    signup: (state, action) => {
      return action.payload;
    },
  },
});

const { reducer, actions } = auth;

export const { restoreToken, signup } = actions;
export default reducer;
